using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Valve.VR.InteractionSystem;

public class LookAt : MonoBehaviour
{
    private Transform LookAtTransform;

    private Vector3 GoalPos;

    [SerializeField] private Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    private float distance;
    void Update()
    {
        GoalPos = Player.instance.hmdTransforms[0].position;
        GoalPos.y = transform.position.y;

        distance = Vector3.Distance(GoalPos, transform.position);
        animator.SetFloat(animator.parameters[0].name, distance);
        transform.LookAt(GoalPos);
    }
}
