using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class RollingGlass : MonoBehaviour
{
    [SerializeField] private ConstantForce glass;
    private Rigidbody glassBody;

    private AudioSource glassAudio;
    // Start is called before the first frame update
    void Start()
    {
        glassBody = glass.GetComponent<Rigidbody>();
        glassAudio = glass.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (glassBody.velocity.magnitude > 1f && !glassAudio.isPlaying)
        {
            glassAudio.Play();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            glass.force = new Vector3(4,0,0);
        }
    }
}
