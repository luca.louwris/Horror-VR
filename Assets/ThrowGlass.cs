using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowGlass : MonoBehaviour
{
    public GameObject glass;

    public int chance ;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private ConstantForce constantForce;

    private float lastThrow;
    // Update is called once per frame
    void Update()
    {
        if (Random.Range(0, chance) == 4 && Time.time - lastThrow < 5)
        {
            lastThrow = Time.time;
            constantForce =Instantiate(glass, transform.position, Quaternion.identity).GetComponent<ConstantForce>();
            StartCoroutine(nameof(throwBottle));
        }
    }

    IEnumerator throwBottle()
    {
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
        constantForce.enabled = false;
    }
}
