using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class PlayerMovement : MonoBehaviour
{
    public SteamVR_Action_Boolean pinchAction = SteamVR_Input.GetAction<SteamVR_Action_Boolean>("GrabPinch");

    private Hand[] hands;

    private Hand currentPointer;

    private Transform currentHandPoint;

    [SerializeField] private Rigidbody playerRigidBody;
    [SerializeField] private CapsuleCollider colliderCheck;

    private Queue<Vector3> AverageSpeed = new Queue<Vector3>(5);
    [SerializeField] private LayerMask layerMask;

    // Start is called before the first frame update
    private void Start()
    {
        hands = Player.instance.hands;
        playerRigidBody = Player.instance.rigSteamVR.GetComponent<Rigidbody>();
        colliderCheck = Player.instance.rigSteamVR.GetComponentInChildren<CapsuleCollider>();
    }

    // Update is called once per frame
    private void Update()
    {
        if (AverageSpeed.Count >= 5)
        {
            AverageSpeed.Dequeue();
        }
        foreach (Hand hand in hands)
        {
            if (WasPinchButtonPressed(hand))
            {
                currentPointer = hand;
            }

            if (IsGrapPinchDown(hand) && currentPointer == hand)
            {
                currentHandPoint = GetPointerTransform(hand);
                AverageSpeed.Enqueue(hand.GetTrackedObjectVelocity());
                AddVelocityToPlayer(CalculateAverage(AverageSpeed));
            }

            if (WasPinchButtonReleased(hand) && currentPointer == hand)
            {
                AverageSpeed.Clear();
                currentPointer = null;
            }
        }
    }

    private Vector3 CalculateAverage(Queue<Vector3> average)
    {
        Vector3 avgSpeed = Vector3.zero;
        foreach (Vector3 vector in AverageSpeed)
        {
            avgSpeed += vector;
        }

        avgSpeed /= average.Count;

        return avgSpeed;
    }

    private void AddVelocityToPlayer(Vector3 handSpeed)
    {
        handSpeed = new Vector3(-handSpeed.x, 0, -handSpeed.z);
        if(SpaceClearCheck())
            playerRigidBody.AddForce(handSpeed * 9);
    }

    [SerializeField] private LineRenderer lineRender;
    private Vector3[] linePositions = new Vector3[2];
    private bool SpaceClearCheck()
    {
        linePositions[0] = colliderCheck.transform.position;
        linePositions[1] = colliderCheck.transform.position + currentHandPoint.forward * 0.75f;
        lineRender.SetPositions(linePositions);
        return !Physics.Raycast(colliderCheck.transform.position, currentHandPoint.forward,0.75f,layerMask);
    }

    private bool WasPinchButtonPressed( Hand hand )
    {
        if ( hand.noSteamVRFallbackCamera != null )
        {
            return Input.GetKeyDown( KeyCode.T );
        }
        return pinchAction.GetStateDown(hand.handType);
    }
    private bool IsGrapPinchDown( Hand hand )
    {
        if ( hand.noSteamVRFallbackCamera != null )
        {
            return Input.GetKey( KeyCode.T );
        }
        return pinchAction.GetState(hand.handType);
    }
    
    private bool WasPinchButtonReleased( Hand hand )
    {
        if ( hand.noSteamVRFallbackCamera != null )
        {
            return Input.GetKeyUp( KeyCode.T );
        }
        return pinchAction.GetStateUp(hand.handType);
    }
    
    private Transform GetPointerTransform( Hand hand )
    {
        if ( hand.noSteamVRFallbackCamera != null )
        {
            return hand.noSteamVRFallbackCamera.transform;
        }
        else
        {
            return hand.transform;
        }
    }
}
