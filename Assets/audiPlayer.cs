using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class audiPlayer : MonoBehaviour
{
    public AudioClip[] monsterClips;

    public AudioSource Source;
    // Start is called before the first frame update
    void playOnce()
    {
        Source.PlayOneShot(monsterClips[Random.Range(0, monsterClips.Length - 1)]);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Monster")
        {
            playOnce();
        }
    }
}
