using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class MonsterFlipAble : MonoBehaviour
{
    public GameObject monster;
    public int enableChance;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Monster")
        {
            int chance = Random.Range(0, 100);
            Debug.Log(chance);
            if(chance < enableChance)
                monster.SetActive(true);
            else
                monster.SetActive(false);
        }
    }
}
